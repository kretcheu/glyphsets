If you are a font developer or typeface designer, see the subdirectory `encodings/GF Glyph Sets` which provides glyph set definition "standards" that are typically useful sets to draw.

On the other hand, the nam files on the `encodings` directory are probably more useful for expert web developers. Those files explain how the Unicode Range subsets are defined, typically per script (writing system), in the Google Fonts css API.
